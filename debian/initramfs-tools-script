#!/bin/sh

PREREQ=""

prereqs()
{
	echo "$PREREQ"
}

case $1 in
# get pre-requisites
prereqs)
	prereqs
	exit 0
	;;
esac

. /scripts/functions

# Disable ICMP ping responses (to let host alive monitoring checks fail)
echo 1 > /proc/sys/net/ipv4/icmp_echo_ignore_all

for device in /sys/bus/pci/drivers/igb/*\.0; do
	if test ! -e "$device"; then
		continue
	fi

	echo "Resetting igb driver for ${device##*/}"
	echo 1 > "${device}/reset"
done

log_begin_msg "Starting networking"
configure_networking || panic "Network configuration failed."
log_end_msg

# Setting up networking
rm -f /etc/resolv.conf
if test -n "$IPV4DNS0"; then
	echo "nameserver $IPV4DNS0" >> /etc/resolv.conf
fi
if test -n "$IPV4DNS1"; then
	echo "nameserver $IPV4DNS1" >> /etc/resolv.conf
fi
if test -n "$DOMAINSEARCH"; then
	echo "search $DOMAINSEARCH" >> /etc/resolv.conf
fi
# shellcheck disable=SC2039
if test -n "$HOSTNAME"; then
	echo "$HOSTNAME" > /etc/hostname
	hostname -F /etc/hostname
	if test -n "$DNSDOMAIN"; then
		cat > /etc/hosts <<EOF
127.0.0.1	localhost
127.0.1.1	$HOSTNAME.$DNSDOMAIN	$HOSTNAME

# The following lines are desirable for IPv6 capable hosts
::1     localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
EOF
	fi
fi

echo "Starting kdump at $(date '+%Y-%m-%d %H:%M:%S %z')..."
kdump-config savecore || panic "'kdump-config savecore' failed with exit code $?."

panic "kdump done at $(date '+%Y-%m-%d %H:%M:%S %z'). You can reboot the system with 'reboot -f' now."
